﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bandwidth.Net;
using Bandwidth.Net.Api;
using apiv2 = Bandwidth.Net.ApiV2;
using Nito.AsyncEx;

namespace Projekt_16782000
{
    class Program
    {
        static string message;
        static apiv2.Message messageV2;

        static void Main(string[] args)
        {
            var client = new Client(
                    Projekt_16782000.Configuration.Default.UserIdAtBandwidthCom // <-- Different than the username to login to the portal
                                                                                // it is located in (upon login) app.bandwidth.com/account/profile, Account tab, Api information section
                    , Projekt_16782000.Configuration.Default.ApiTokenAtBandwidthCom
                    , Projekt_16782000.Configuration.Default.ApiSecretAtBandiwdthCom
                );


            AsyncContext.Run(() => SetMessage(client));
            Console.WriteLine($"Message Id is {message}");
            MessageQuery messageGetter = new MessageQuery();


            var messages = client.Message.List();
            foreach (var msg in messages)
                Console.WriteLine(msg.Text);


        }

        private static async void SetMessage(Client client)
        {
                message = await client.Message.SendAsync(
                    new MessageData
                    {
                        From = "+12403906700", // This must be a Bandwidth number on the Bandwidth.com account
                        To = "19402318022",
                        Text = "Ahoy, mateys!!!"
                    });
          
        }

        //There is a Version 2.x of the API, this is the code to use.
        /*private static async void SetMessageV2(Client client)
        {
            messageV2 = await client.V2.Message.SendAsync(
                new apiv2.MessageData
                {
                    From = "+12403906700", // This must be a Bandwidth number on the Bandwidth.com account
                    To = new string[] { "+19402318022" },
                    Text = "Hello world!"
                });
        }*/
    
    }
}
